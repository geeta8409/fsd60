Day-02
------
DDL - Create, Alter, Drop
DML - Insert, Update, Delete
DQL - Select



create table student (
sid int(4), 
sname varchar(20), 
age int, 
course varchar(10), 
fees double(8, 2)
);


show tables;

Get the Student table description
desc student;

insert a record into the student table
insert into student values (101, 'Harsha', 22, 'MySQL', 2500.00);
insert into student values (102, 'Vamsi',  42, 'Java',  3500.00);
insert into student values (103, 'Pavan',  62, 'MERN',  4500.00);


Select * from Student;

Add a new column(mobile) to the existing student table
alter table student add mobile varchar(20);

Remove a column from the exising student table
alter table student drop column mobile;

Delete the student table

delete from student;
--it deletes all records from the student table

show tables;
+-----------------+
| Tables_in_fsd60 |
+-----------------+
| student         |
+-----------------+

--To delete the student table completely from the database
drop table student;

show tables;


show databases;
drop database fsd60;
show databases;

DDL - Create, Alter, Drop




create table student (
sid int(4), 
sname varchar(20), 
age int, 
course varchar(10), 
fees double(8, 2)
);

insert into student values (101, 'Harsha', 22, 'MySQL', 2500.00);
insert into student values (102, 'Vamsi',  42, 'Java',  3500.00);
insert into student values (103, 'Pavan',  62, 'MERN',  4500.00);

Select * from Student;



insert into student (sid, sname, age) values (104, 'pasha', 55);
select * from student;

insert into student (course, sid, fees, age, sname) values ('C', 105, 6500.00, 23, 'vinay');
select * from student;

insert into student (course, sid, fees, age, sname) values (null, 106, null, 22, 'kiran');
select * from student;




insert multiple records using single insert query
-------------------------------------------------

insert into student values 
(107, 'Sai', 22, 'C++', 2500.00),
(108, 'Sonu', 23, 'Java', 3500.00),
(109, 'Srikanth', 24, 'Oracle', 4500.00);


select * from student;



Update
------
update student set fees = 5000.00;

update student set course='Java' where sid = 104;
update student set course='C' where sname='kiran';
update student set course='Java', fees='3500.00' where sid=101;
update student set fees=2500.00 where course='C';
update student set fees = (fees + 200);



Delete
------
delete from student where sid=101;
delete from student where sname='Srikanth';
delete from student;


Select
------
select * from student;
select sid, sname from student;
select sname, sid from student;
select sname, course from student;

select * from student where sid = 101;
select * from student where sname = 'harsha';

select * from student where course = 'java';

select * from student where age < 24;
select * from student where age <= 24;

select * from student where age >= 23 and age <= 24;
select * from student where age > 22 and age < 25;





Constraints in SQL
------------------
1. Not Null
2. Unique
3. Primary Key
4. Foreign Key
5. Check
6. Default



1. Not Null
-----------
Create Table Student1 (sid int not null, age int);
desc student1;
insert into student1 values (101, 22);
insert into student1 values (101, 23);
select * from student1;

insert into student1 values (null, 25);
ERROR 1048 (23000): Column 'sid' cannot be null

insert into student1 (sid, age) values (null, 22);
ERROR 1048 (23000): Column 'sid' cannot be null



2. Unique
---------

Drop table student1;

create table student1 (sid int unique key, age int);
desc student1;

insert into student1 values (101, 22);

insert into student1 values (101, 23);
ERROR 1062 (23000): Duplicate entry '101' for key 'student1.sid'

insert into student1 values (null, 23);
insert into student1 values (null, 24);

Select * from student1;




Unique and Not Null
-------------------
Drop table student1;

create table student1 (sid int unique key not null, age int);

desc student1;

insert into student1 values (101, 22);
Query OK, 1 row affected (0.01 sec)

insert into student1 values (101, 23);
ERROR 1062 (23000): Duplicate entry '101' for key 'student1.sid'

insert into student1 values (null, 23);
ERROR 1048 (23000): Column 'sid' cannot be null

select * from student1;




3. Primary Key (Combination of Unique + Not Null)
--------------
Drop table student1;

create table student1 (sid int primary key, age int);

desc student1;

insert into student1 values (101, 22);

insert into student1 values (101, 23);
ERROR 1062 (23000): Duplicate entry '101' for key 'student1.PRIMARY'

insert into student1 values (null, 23);
ERROR 1048 (23000): Column 'sid' cannot be null

select * from student1;






4. Check
--------
Drop table student1;

create table student1 (sid int, age int check (age > 12 and age < 20));

insert into student1 values (101, 15);

insert into student1 values (102, 10);
ERROR 3819 (HY000): Check constraint 'student1_chk_1' is violated.

insert into student1 values (102, 20);
ERROR 3819 (HY000): Check constraint 'student1_chk_1' is violated.

insert into student1 values (102, 18);

select * from student1;





5. Default
----------
Drop table student1;

create table student1 (sid int, age int default 19);

desc student1;

insert into student1 values (101, 22);

insert into student1 values (102, 23);

insert into student1(sid) values (103);

select * from student1;